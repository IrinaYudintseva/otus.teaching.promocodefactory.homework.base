﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task<T> AddAsync(T item) 
        {
            if (Data == null) 
                Data = new List<T>();

            item.Id = Guid.NewGuid();
            var list = Data.ToList();
            list.Add(item);
            Data = list.AsEnumerable();

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item .Id));
        }

        public Task<T> EditAsync(T item)
        {
            if (Data == null) 
                return Task.FromResult<T>(null);

            var dataItem = Data.FirstOrDefault(x => x.Id == item.Id);
            var dataList = Data.ToList();
            int index = dataList.IndexOf(item);
            dataList[index] = item;
            Data = dataList.AsEnumerable<T>();

            return Task.FromResult(Data.FirstOrDefault(x => x.Id == item.Id));
        }

        public Task<bool> DeleteAsync(Guid id)
        {
            if (Data == null) 
                return Task.FromResult(true);

            var dataItem = Data.FirstOrDefault(x => x.Id == id);
            if (dataItem == null)
               return Task.FromResult(true);

            var dataList = Data.ToList();
            dataList.Remove(dataItem);

            Data = dataList.AsEnumerable<T>();
            return Task.FromResult(true);
        }
    }
}