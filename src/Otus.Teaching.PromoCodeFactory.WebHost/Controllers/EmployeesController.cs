﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IRepository<Employee> _employeeRepository;

        public EmployeesController(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            return CreateEmployeeResponse(employee);
        }

        
        /// <summary>
        /// Добавить нового сотрудника 
        /// </summary>
        /// <returns></returns>               
        [HttpPost]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeAsync([FromBody] EmployeeShortRequest employeeData)
        {
            var employee = new Employee()
            {
                FirstName = employeeData.FirstName,
                LastName = employeeData.LastName,
                Email = employeeData.Email,
                AppliedPromocodesCount = employeeData.AppliedPromocodesCount,
                Roles = new List<Role>()
            };

            var new_employee = await _employeeRepository.AddAsync(employee);
            if (new_employee == null)
                return BadRequest();

            return CreateEmployeeResponse(new_employee);
        }
        
        /// <summary>
        /// Удалить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteEmployeeAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);
            if (employee == null)
                return NotFound();

            if (!await _employeeRepository.DeleteAsync(id))
                return BadRequest();

            return Ok();
        }

        /// <summary>
        /// Изменить данные сотрудника 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> EditEmployeeAsync(Guid id, [FromBody] EmployeeShortRequest employeeData)
        {
            var updEmployee = await _employeeRepository.GetByIdAsync(id);
            if (updEmployee == null)
                return NotFound();

            updEmployee.FirstName = employeeData.FirstName;
            updEmployee.LastName = employeeData.LastName;
            updEmployee.Email = employeeData.Email;
            updEmployee.AppliedPromocodesCount = employeeData.AppliedPromocodesCount;

            updEmployee = await _employeeRepository.EditAsync(updEmployee);

            if (updEmployee == null)
                return BadRequest();

            return CreateEmployeeResponse(updEmployee);
        }        

        /// <summary>
        /// Добавить роль сотруднику 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{employeeId:guid}/roles")]
        public async Task<ActionResult<EmployeeResponse>> AddEmployeeRoleAsync(Guid employeeId, [FromBody] Role role)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee == null)
                return NotFound();

            if (employee.Roles == null)
                employee.Roles = new List<Role>();
            else if (employee.Roles.AsEnumerable().Any(x => x.Id == role.Id))
                return CreateEmployeeResponse(employee);

            employee.Roles.Add(role);

            var updEmployee = await _employeeRepository.EditAsync(employee);
            if (updEmployee == null)
                return NotFound();

            return CreateEmployeeResponse(updEmployee);
        }

        /// <summary>
        /// Удалить роль у сотрудника 
        /// </summary>
        /// <returns></returns>
        [HttpPut("{employeeId:guid}/roles/{roleID:guid}")]
        public async Task<ActionResult<EmployeeResponse>> DeleteEmployeeRoleAsync([FromRoute] Guid employeeId, [FromRoute] Guid roleID)
        {
            var employee = await _employeeRepository.GetByIdAsync(employeeId);
            if (employee == null)
                return NotFound();

            if(employee.Roles == null)
                return CreateEmployeeResponse(employee);

            var role = employee.Roles.AsEnumerable().FirstOrDefault(x => x.Id == roleID);
            if (role == null)
                return CreateEmployeeResponse(employee);

            employee.Roles.Remove(role);

            var updEmployee = await _employeeRepository.EditAsync(employee);
            if (updEmployee == null)
                return NotFound();

            return CreateEmployeeResponse(updEmployee);
        }

        private static EmployeeResponse CreateEmployeeResponse(Employee employee) 
        {
            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Roles = employee.Roles.Select(x => new RoleItemResponse()
                {
                    Id = x.Id,
                    Name = x.Name,
                    Description = x.Description
                }).ToList(),
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }
    }
}